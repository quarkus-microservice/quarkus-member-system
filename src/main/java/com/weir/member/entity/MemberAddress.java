package com.weir.member.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

/**
 * 会员地址表
 * @author weir
 *
 */
@Entity
@Table(name = "member_address")
public class MemberAddress extends BaseEntity {
	
	private static final long serialVersionUID = -5620306830094420254L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long id;
	
	@Column(name="member_id")
	public Long memberId;
	public String name;
	public String phone;
	@Column(name="post_code")
	public String postCode;
	public String province;
	public String city;
	public String region;
	@Column(name="detail_address")
	public String detailAddress;
	public String areacode;
	@Column(name="default_status")
	public Integer defaultStatus;

}
