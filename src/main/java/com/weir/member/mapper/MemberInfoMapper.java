package com.weir.member.mapper;

import org.mapstruct.Mapper;

import com.weir.member.entity.MemberInfo;
import com.weir.quarkus.commons.vo.member.MemberInfoVo;

@Mapper(componentModel = "cdi")
public interface MemberInfoMapper {

	MemberInfoVo toVo(MemberInfo memberInfo);
}
