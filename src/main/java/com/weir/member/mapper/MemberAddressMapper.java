package com.weir.member.mapper;

import org.mapstruct.Mapper;

import com.weir.member.entity.MemberAddress;
import com.weir.quarkus.commons.vo.member.MemberAddressVo;

@Mapper(componentModel = "cdi")
public interface MemberAddressMapper {

	MemberAddressVo toVo(MemberAddress memberAddress);
}
