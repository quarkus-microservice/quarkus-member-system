package com.weir.member.resource;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import com.weir.member.entity.MemberAddress;
import com.weir.member.entity.MemberInfo;
import com.weir.member.log.Log;
import com.weir.member.mapper.MemberAddressMapper;
import com.weir.member.mapper.MemberInfoMapper;
import com.weir.member.util.Sequence;
import com.weir.quarkus.commons.vo.member.MemberAddressVo;
import com.weir.quarkus.commons.vo.member.MemberInfoVo;
import io.quarkus.panache.common.Sort;
import io.quarkus.panache.common.Sort.Direction;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;

@Tag(name = "会员管理")
@Path("member/info")
@ApplicationScoped
public class MemberInfoResource {
	
	@Inject MemberInfoMapper mapper;
	@Inject MemberAddressMapper addressMapper;

	@Log(value = "获取会员及地址")
	@Operation(summary = "获取会员及地址")
	@GET
	@Path("address/list/{memberId}")
	public MemberInfoVo getAddress(@PathParam("memberId") Long memberId) {
		MemberInfo memberInfo = MemberInfo.findById(memberId);
		MemberInfoVo memberInfoVo = mapper.toVo(memberInfo);
		// 查询该会员的所有地址
		List<MemberAddress> list2 = MemberAddress.list("memberId", Sort.by("defaultStatus",Direction.Descending), memberId);
		List<MemberAddressVo> addressVos = list2.stream().map(d -> addressMapper.toVo(d)).collect(Collectors.toList());	
		memberInfoVo.addressVos = addressVos;
		return memberInfoVo;
	}
	
	@POST
	@Path("add")
	@Transactional
	public MemberInfo add(MemberInfo m2) {
		long a = System.currentTimeMillis();
		Sequence sequence = new Sequence(0,0);
//		List<Long> list = new ArrayList<>();
		for (int i = 0; i < 50000; i++) {
			long id = sequence.nextId();
//			System.out.println("----------------------"+ id);
			MemberInfo m = new MemberInfo();
			m.id=id;
			m.username = m2.username + i;
			m.password = m2.password + i;
			m.phone = m2.phone + i;
			m.email = m2.email + i;
			m.persist();
//			list.add(id);
		}
//		Map<Long, Long> collect = list.stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
		
//		for(Long s:collect.keySet()){
//            if(collect.get(s) > 1){
//                System.out.println("-------------"+s+" ");
//            }
//        }
		long b = System.currentTimeMillis();
		System.out.println("-------------"+(b-a));
		return m2;
	}
	
}
