package com.weir.member;
import io.quarkus.runtime.annotations.QuarkusMain;
import io.quarkus.runtime.Quarkus;

@QuarkusMain  
public class MemberMain {

    public static void main(String ... args) {
        System.out.println("Running member main method");
        Quarkus.run(args); 
    }
}